#!/bin/sh

icons_dir="${HOME}"/.local/share/icons
themes_dir="${HOME}"/.local/share/themes
#gtk4_dir="${XDG_CONFIG_HOME:-HOME/.config}"/gtk-4.0
current_dir="$(pwd)"
gruvbox_bl="${current_dir}"/themes/Gruvbox-Dark-BL
gruvbox_icons="${current_dir}"/icons/Gruvbox-Dark

if [ "$0" = install.sh ]; then
    echo 'Must run script from inside project root directory'
    exit 1
fi

[ -d "${icons_dir}"  ] || mkdir -pv "${icons_dir}"
[ -d "${themes_dir}" ] || mkdir -pv "${themes_dir}"
#[ -d "${gtk4_dir}"   ] || mkdir -pv "${gtk4_dir}"

ln -sv  "${gruvbox_icons}" "${icons_dir}"/
ln -sv  "${gruvbox_bl}"    "${themes_dir}"/
#ln -svt "${gtk4_dir}"      "${gruvbox_bl}/gtk-4.0/assets" \
#                           "${gruvbox_bl}"/gtk-4.0/gtk*.css
